# What is this? #

A simple Ecommerce React Native Application [FrontEnd - not connected to any BE (yet).]


### What is this repository for? ###

* A react native application focusing on simple ecommerce for a clothing brand.

### Things needed to be done ###

* Quantity Validation for item stock for each Varients
* Color Varients / Price
* Checkout function
* Product Page

### Built with ###

* React Native cli / Typescript
* React Redux
* Android studio

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact