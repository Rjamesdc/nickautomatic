import React from "react";
import { View, Text, LogBox, StatusBar } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from "./navigation/GlobalNavigation";
import { Main } from "./navigation/Main";
import 'react-native-gesture-handler';
import { COLORS } from "./constants";

/* REDUX */ 
import { Provider } from "react-redux";
import store from "./redux/storeConfig";

const App = () => {
    return (
        <>
        <StatusBar
          barStyle={"dark-content"}
          translucent={true}
          backgroundColor={COLORS.transparent}
        />
         <NavigationContainer ref={navigationRef}>
        <Provider store={store}>
          <Main />
        </Provider>
      </NavigationContainer>
     </>
    )
}

export default App