import React, { FC } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Tabs from './BottomNavigation';
import { ProductInfo, Cart } from '../screens';

const NavigationStack:any = createNativeStackNavigator();

interface Props {
  title?: string;
  color?: string;
  style?: any;
}

export const Main: FC<Props> = () => (
  <NavigationStack.Navigator
    screenOptions={{
      headerShown: false
    }}>
    <NavigationStack.Screen
      name='App'
      component={Tabs}
    />
    {/* Stack Screens */}
    <NavigationStack.Screen
      name='ProductInfo'
      component={ProductInfo}
    />
    <NavigationStack.Screen
      name='Cart'
      component={Cart}
    />
  </NavigationStack.Navigator>
);
