import React, { FunctionComponent } from "react";
import { View, Text, Image } from "react-native";
import { COLORS } from "../constants";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

interface Props {
    icon: any,
    focused: any
    size: number
}

 const TabarIcons:FunctionComponent<Props>= (props:Props) => {
     const { icon, focused, size } = props;
  return (
    <View
      style={{
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 10,
        height: 60,
        width: 40
      }}
    >
        <Icon 
        style={{ marginBottom: 5 }}
        name={icon}
        size={focused ? size: 20}
        color={focused ? COLORS.white : COLORS.gray}
        />
        <View style={{
          paddingHorizontal: 5,
          paddingVertical: 1,
          backgroundColor: focused ? COLORS.white : COLORS.black
        }}/>
    </View>
  );
}

export default TabarIcons;