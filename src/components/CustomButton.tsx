import React, { FC } from 'react'
import { 
    View, 
    Text,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'
import { COLORS, FONTS, SIZES } from '../constants'

interface CustomButton {
    title: string,
    action: () => void,
    customStyle?: any

}

const CustomButton:FC<CustomButton> = (props:CustomButton) => {
    const { action, title, customStyle } = props;    
    return (
        <View style={styles.container}>
            <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => action()}>
            <Text style={[styles.title]}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        zIndex: 999,
    },
    title:{
        backgroundColor: COLORS.blue,
        width: 50
    }
})

export default CustomButton
