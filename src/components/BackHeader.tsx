import React, {FC} from 'react'
import { 
    View, 
    Text,
    StyleSheet,
    TouchableOpacity,
    Image
 } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { COLORS, FONTS, SIZES } from '../constants'
import * as Navigation from '../navigation/GlobalNavigation'

interface BackHeader {
    action: () => void;
    showCart?: boolean;
    showShare?: boolean
}

const BackHeader:FC<BackHeader> = (props:BackHeader) => {
    const { action, showCart, showShare } = props;
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => action()}>
                <Image 
                resizeMethod="resize"
                style={{
                    top: 15,
                    left: 20,
                    width: 20,
                    height: 20
                }}
                source={require('../assets/icons/back.png')}/>
            </TouchableOpacity>
            {
                showCart &&
                <TouchableOpacity onPress={() => Navigation.navigate("Cart",{})}>
                <Icon 
                 name="cart"
                 size={25}
                 color={COLORS.black}
                 style={{
                     position: 'absolute',
                     right: 30,
                     top: -9
                 }}
                />
             </TouchableOpacity>
            }
            {
                showShare &&
                <TouchableOpacity onPress={() => console.log("Shared!")}>
                <Icon 
                 name="share-variant"
                 size={25}
                 color={COLORS.black}
                 style={{
                     position: 'absolute',
                     right: 30,
                     top: -9
                 }}
                />
             </TouchableOpacity>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        height: SIZES.height / 8,
        width: SIZES.width,
        justifyContent: 'center',
    }
})

export default BackHeader
