import React, {FC} from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

interface Stars {
  rating: number;
}

const StarRating: FC<Stars> = (props: Stars) => {
  const {rating} = props;
  return (
    <View
      style={{
        flexDirection: 'row',
      }}>
      {rating === 1 && (
        <Icon
          style={{
            marginTop: 5,
            color: 'orange',
          }}
          name="star"
          size={15}
        />
      )}
      {rating === 2 && (
        <>
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
        </>
      )}
      {rating === 3 && (
        <>
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
        </>
      )}
      {rating === 4 && (
        <>
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
        </>
      )}
      {rating === 5 && (
        <>
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
          <Icon
            style={{
              marginTop: 5,
              color: 'orange',
            }}
            name="star"
            size={15}
          />
        </>
      )}
    </View>
  );
};

export default StarRating;
