import React, {FC} from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { COLORS, FONTS, SIZES } from '../constants'

interface Featured {
    data: any,
    index: number,
    action: any,
}

const FeaturedLists:FC<Featured> = (props: Featured) => {
    const { data, action, index } = props;
    return (
        <View style={{
            paddingHorizontal: 10
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            height: 300,
            width: 200,
            marginRight: 5,
            borderRadius: 20,
            overflow:'hidden'
          }}
          onPress={() => action()}
        >
          <Image
            source={{uri: data.image}}
            resizeMethod="resize"
            style={{
              width: 200,
              height: 250,
              borderRadius: SIZES.radius,
            }}
          />
          <View style={styles.categoryCard}>
            <Text style={{
                color: COLORS.white
            }}>
                {data.category}
            </Text>
          </View>
    
          <View style={styles.infoCard}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Text style={styles.productName}>
                  {data.productName}
              </Text>
            </View>
            <Text style={{
                color: COLORS.black,
                fontFamily: FONTS.regular
            }}>
                Price: {data.prices[0].small}
            </Text>
          </View>
        </TouchableOpacity>
        </View>
      );
    
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red'
    },
    infoCard:{
        position: "absolute",
        height: 80,
        width: SIZES.width,
        paddingHorizontal: 10,
        paddingTop: 5,
        bottom: 0,
        backgroundColor: COLORS.white,
        borderRadius: SIZES.radius,
    },
    productName:{
        color: COLORS.black,
        fontFamily: FONTS.bold,
        width: '40%'
    },
    categoryCard:{
        position: "absolute",
        top: 20,
        left: 10,
        backgroundColor: COLORS.transparentDarkGray,
        paddingHorizontal: SIZES.radius,
        paddingVertical: 10,
        borderRadius: 15,
    }
})

export default FeaturedLists
