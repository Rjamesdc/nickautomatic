import React from 'react'
import { View, Text, StyleSheet, Image, ImageBackground } from 'react-native'
import { SIZES, COLORS, FONTS } from '../constants'
import LinearGradient from 'react-native-linear-gradient'

const Products = () => {
    return (
        <View style={styles.container}>
            <ImageBackground
            style={{
                width: SIZES.width,
                height: SIZES.height
            }}
            source={require('../assets/images/nckbanner2.jpg')}
            >
        <LinearGradient
            colors={[COLORS.black, COLORS.transparentBlack9]}
            style={{
                opacity: 0.8,
                width: SIZES.width,
                height: SIZES.height,
                position:'absolute',
                zIndex: 1
            }}
          />
          <View style={{
              zIndex: 999,
              alignItems:'center'
            }}>
          <Text style={{
              fontFamily:FONTS.bold,
              fontSize: 30,
              top: SIZES.height / 2,
              color: COLORS.white
          }}>Coming Soon</Text> 
        </View>
        </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    }
})

export default Products
