import React, {FC} from 'react'
import { 
    View, 
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    ImageBackground,
    FlatList,
    StatusBar,
    SafeAreaView
} from 'react-native'
import { useDispatch, connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import { COLORS, SIZES, FONTS } from '../constants'
import { featuredData } from '../utils/dummyData';
import FeaturedLists from '../components/FeaturedLists'
import { StarRating } from '../components'
import * as Navigation from '../navigation/GlobalNavigation'

interface Props {

}

const Home:FC<Props> = (props:Props) => {

    const HomeHeader = () => {
        return(
            <View>
                <StatusBar barStyle="light-content"/>
                <ImageBackground style={styles.bannerStyle} source={require('../assets/images/nckbanner1.jpg')}>
                    <View style={styles.bannerItems}>
                        <Text style={styles.bannerText}>The No Good Crew Sale</Text>
                        <TouchableOpacity 
                        activeOpacity={0.7}
                        style={styles.bannerBtn}>
                            <Text style={{fontFamily: FONTS.regular}}>Check</Text>
                        </TouchableOpacity>
                    </View>
                    <LinearGradient
                    colors={[COLORS.transparent,COLORS.black]}
                    style={styles.bannerGradient}
                />
                </ImageBackground>
            </View>
        )
}

const FeatureLists = () => {
    const { products }:any = props;
    return(
        <View>
            <Text>
                </Text>
        <Text style={styles.featuredHeader}>Featured Items</Text>
           <FlatList 
           showsHorizontalScrollIndicator={false}
           data={products}
           horizontal
           keyExtractor={(item:any) => item.id}
           renderItem={({item,index}:any) => <FeaturedLists data={item} index={index} action={() => Navigation.navigate("ProductInfo", {product:item})}/>}
           />
           <Text style={styles.wsHeader}>What's New</Text>
        </View>
    )
}

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
            data={featuredData}
            ListHeaderComponent={
                <View>
                {HomeHeader()}
                {FeatureLists()}
                </View>
            }
            keyExtractor={(item:any) => item.id}
            ListFooterComponent={<SafeAreaView style={{ marginBottom: 100 }} />}
            renderItem={({item, index}:any) => 
                <View style={styles.wsItems}>
                   <TouchableOpacity style={{
                        flexDirection: "row",
                        padding: 10,
                        borderRadius: SIZES.radius,
                    }}
                    >
                        <Image
                        source={{uri: item.image}}
                        resizeMode="cover"
                        style={{
                            width: 100,
                            height: 100,
                            borderRadius: SIZES.radius
                        }}
                        />

                        <View style={{
                            width: "75%",
                            paddingHorizontal: 20,
                        }}>
                            <Text style={{
                            fontFamily: FONTS.bold,
                            fontSize: 18,
                            color: COLORS.black
                            }}>{item.productName}</Text>
                            <Text style={{
                                color: COLORS.black,
                                fontFamily: FONTS.regular,
                                fontSize: 15
                            }}>Price: {item.defaultPrice}</Text>
                            <StarRating rating={item.rating}/>
                            <Text style={styles.categoryText}>
                                {item.category}
                            </Text>
                        </View>

                    </TouchableOpacity>
                </View>
            }
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.nickbg
    },
    bannerStyle:{
        height: SIZES.height / 2
    },
    bannerItems:{
        width: SIZES.width / 1.5,
        paddingHorizontal: 15,
        position:'absolute',
        bottom: 40,
    },
    bannerText:{
        color: COLORS.white,
        fontFamily: FONTS.bold,
        fontSize: 30,
        width: '100%',
        zIndex: 1
    },
    bannerGradient:{
        opacity: 0.5,
        position: 'absolute',
        bottom:0,
        width: '100%',
        height: '100%',
        paddingVertical: 25,
        paddingHorizontal: 20,
        alignItems: 'center',
    },
    bannerBtn:{
        backgroundColor: 'orange',
        marginTop: 10,
        width: '45%',
        zIndex: 2,
        paddingVertical: 10,
        alignItems: 'center'
    },
    featuredHeader:{
        paddingHorizontal: 10,
        marginVertical: 10,
        fontFamily: FONTS.bold,
        fontSize: 20,
        color: COLORS.black
    },
    wsHeader:{
        paddingHorizontal: 10,
        paddingVertical: 10,
        color: COLORS.black,
        fontFamily: FONTS.bold,
        fontSize: 20
    },
    wsItems:{
        paddingHorizontal: 10
    },
    categoryText:{
        width: 50,
        color: COLORS.white,
        fontSize: 10,
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: COLORS.transparentBlack9
    }
})

const mapStateToProps = (state: any) => ({
    products: state.productReducer.products,
  });
  
  const mapDispatchToProps = (dispatch: any) => {
    return {
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Home);