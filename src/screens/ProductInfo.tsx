import React, { FC, useState, useEffect } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StatusBar,
    StyleSheet,
    ScrollView
} from 'react-native'
import BackHeader from '../components/BackHeader'
import { useDispatch } from 'react-redux'
import * as Navigation from '../navigation/GlobalNavigation'
import StarRating from '../components/StarRating'
import { COLORS, FONTS, SIZES } from '../constants'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { types } from '../redux/Products/types'

interface Product {
    product: any
    route: any
}

const ProductInfo: FC<Product> = (props: Product) => {
    const dispatch = useDispatch()
    const { product } = props.route.params;
    const [quantity, setQuantity] = useState(1);
    const [selectedPrice, setSelectedPrice] = useState(0);
    const [selectedVarient, setSelectedVarient] = useState("");

    useEffect(() => {
        setSelectedVarient(product.sizeVarient[0])
    }, [])

    useEffect(() => {
        const price = product.prices.map((x:any) => x[selectedVarient]).pop();
        setSelectedPrice(price)
    }, [selectedVarient])


    const addToCart = (productDetails:any) => {
        const selectedItem = {
            id: productDetails.id,
            varient: selectedVarient,
            quantity,
            price: selectedPrice,
            productName: productDetails.productName,
            image: productDetails.image,
            stock: productDetails.stock
        }
        dispatch({
            type: types.ADD_TO_CART,
            payload: selectedItem
        })
    }

    return (
        <>
            <StatusBar barStyle="dark-content" backgroundColor="transparent" />
            <BackHeader showShare={false} showCart={true} action={() => Navigation.navigate("Home", {})} />
            <ScrollView style={styles.container}>
                <Image
                    resizeMethod="resize"
                    style={{
                        width: SIZES.width,
                        height: SIZES.height / 2
                    }}
                    source={{ uri: product.image }} />
                <View style={styles.productInfo}>
                    <Text style={styles.productName}>{product.productName}</Text>
                    <Text style={styles.productPrice}>PHP {selectedPrice}</Text>
                    <View style={styles.rating}>
                        <StarRating rating={product.rating} />
                        <Text style={styles.ratingValue}>{product.rating}/5</Text>
                    </View>
                    <Text style={{
                        fontFamily: FONTS.regular,
                        color: COLORS.black
                    }}>Varients & Sizes</Text>
                    <View style={{
                        flexDirection: 'row',
                        marginBottom: 10
                    }}>
                    {
                        product.sizeVarient.map((sizes:any, index:number) => 
                            <TouchableOpacity key={index} 
                            activeOpacity={0.9}
                            onPress={() => setSelectedVarient(sizes)}
                            style={{
                                marginRight: 10,
                                marginTop: 5,
                                paddingHorizontal: 20,
                                paddingVertical: 5,
                                backgroundColor: selectedVarient === sizes ? '#fc6f03' : COLORS.transparentBlack3
                            }}>
                                <Text style={{
                                    color: selectedVarient === sizes ? COLORS.gray2 : COLORS.transparentBlack5
                                }}>{sizes}</Text>
                            </TouchableOpacity>
                        )
                    }
                </View>
                <Text style={styles.productDescription}>
                    {product.description}
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </Text>
                </View>
            </ScrollView>
            <View style={styles.option}>
                    <TouchableOpacity
                    onPress={() => {
                        const productDetails = { 
                            id: product.id,
                            productName: product.productName,
                            image: product.image,
                            stock: product.stock
                        }
                        addToCart(productDetails)
                    }}
                    disabled={product.stock === 0 && true}
                    activeOpacity={0.9}
                    style={[styles.addCartBtn, {
                    backgroundColor: product.stock === 0 ? COLORS.gray : COLORS.black,
                    }]}>
                        <Text style={{
                            fontFamily: FONTS.bold,
                            color: COLORS.white
                            }}>{product.stock === 0 ? "Out of Stock" : "Add to cart"}</Text>
                    </TouchableOpacity>
                    <View style={{
                        flexDirection: 'row',
                        position: 'absolute',
                        right: 20
                    }}>
                    <TouchableOpacity
                    disabled={(quantity === 1 || product.stock === 0 ? true : false)}
                    onPress={() => setQuantity((prev) => prev - 1)}
                    activeOpacity={0.9}
                    style={styles.negative}>
                        <Icon 
                        name="minus-circle"
                        size={35}
                        color={quantity === 1 || product.stock === 0 ? COLORS.gray : COLORS.black}
                        />
                    </TouchableOpacity>
                    <Text style={{
                        top: 5,
                        fontFamily: FONTS.bold
                    }}>{quantity}</Text>
                    <TouchableOpacity 
                    disabled={(product.stock === 0 || product.stock === quantity ? true : false)}
                    onPress={() => setQuantity((prev) => prev + 1)}
                    activeOpacity={0.9}
                    style={styles.positive}>
                        <Icon 
                        name="plus-circle"
                        size={35}
                        color={product.stock === 0 || product.stock === quantity ? COLORS.gray : COLORS.black}
                        />
                    </TouchableOpacity>
                    </View>
                </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10
    },
    productInfo: {
        marginTop: 10
    },
    productName: {
        fontFamily: FONTS.bold,
        fontSize: 25,
        color: COLORS.black
    },
    productPrice: {
        fontFamily: FONTS.bold,
        fontSize: 18,
        color: COLORS.black
    },
    rating: {
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 10
    },
    ratingValue: {
        fontFamily: FONTS.regular, top: 2, left: 5
    },
    productDescription:{
        fontFamily: FONTS.regular,
        color: COLORS.black
    },
    addCartBtn: {
        paddingHorizontal: 80,
        paddingVertical: 8,
        borderRadius: 10,
        marginRight: 20
    },
    option:{
        width: SIZES.width,
        height: SIZES.height / 13,
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    negative:{
        marginRight: 10,
    },
    positive:{
        marginLeft: 10
    }
})

export default ProductInfo
