import React, {FC} from 'react'
import { 
    View, 
    Text, 
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native'
import { connect, useDispatch } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { FONTS, COLORS, SIZES } from '../constants'
import { types } from '../redux/Products/types'
import BackHeader from '../components/BackHeader'
import * as Navigation from '../navigation/GlobalNavigation'

interface Cart {
    cartItems: any,
    total: number
}

const Cart:FC<Cart> = (props:Cart) => {
    const { cartItems, total } = props;
    const dispatch = useDispatch()

    const addQuantity = (id:any, varient:any) => {
        const itemDetails = { id, varient }
        dispatch({
            type: types.ADD_QUANTITY,
            payload: itemDetails
        })
    }

    const decreaseQuantity = (item:any) =>{
        dispatch({
            type: types.DECREASE_QUANTITY,
            payload: item
        })
    }

    return (
        <>
         <BackHeader showShare={true} showCart={false} action={() => Navigation.goBack()}/>
            <ScrollView style={styles.container}>
                <Text style={styles.cartHeader}>Shopping Cart</Text>
                <Text style={{
                    fontFamily: FONTS.regular,
                    textDecorationLine: 'underline'
                }}>{!cartItems.length ? "Your cart is empty!" : `${cartItems.length} items`}</Text>
                <View style={styles.horizontalLine}/>
                <View>
                  {
                      cartItems.map((item:any, index:number) =>
                      <View style={{
                          paddingVertical: 10,
                          marginVertical: 10,
                          flexDirection: 'row',
                          overflow: 'hidden'
                      }} key={item.id + index}>
                          <TouchableOpacity
                          activeOpacity={0.7}
                          style={{
                            position:'absolute',
                            right: 0
                             }}
                          onPress={() => dispatch({
                              type: types.REMOVE_ITEM,
                              payload: item
                          })}>
                          <Icon 
                          name="close-circle"
                          size={20}
                          color={'#fc383b'}
                          />
                          </TouchableOpacity>
                      <Image 
                      resizeMethod="resize"
                      style={{
                          width: 100,
                          height: 100
                      }}
                      source={{uri: item.image }}/>
                      <View style={{
                          marginLeft: 10
                      }}>
                          <Text style={{
                              fontFamily: FONTS.bold,
                              fontSize: 17,
                              width: '100%',
                              color: COLORS.black
                          }}>{item.productName}</Text>
                          <Text>Varient: {item.varient}</Text>
                          <View style={{
                              marginTop: 20,
                              flexDirection: 'row',
                          }}>
                              <Text style={{
                                  marginRight: 50,
                                  fontSize: 17,
                                  color: COLORS.black,
                                  fontFamily: FONTS.regular
                              }}>PHP {item.price}</Text>
                              <TouchableOpacity
                                onPress={() => decreaseQuantity(item)}
                                activeOpacity={0.9}
                                style={styles.negative}>
                                    <Icon 
                                    name="minus-circle"
                                    size={35}
                                    color={COLORS.black}
                                    />
                                </TouchableOpacity>
                                <Text style={{
                                    top: 5,
                                    fontFamily: FONTS.bold
                                }}>{item.quantity}</Text>
                                <TouchableOpacity 
                                disabled={(item.quantity === item.stock ? true : false)}
                                onPress={() => addQuantity(item.id, item.varient)}
                                activeOpacity={0.9}
                                style={styles.positive}>
                                    <Icon 
                                    name="plus-circle"
                                    size={35}
                                    color={item.stock === item.quantity ? COLORS.gray : COLORS.black}
                                    />
                                </TouchableOpacity>
                              </View>
                              </View>
                          </View>
                      )
                  }
                </View>
                <View style={styles.horizontalLine}/>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 10,
                    marginBottom: 20
                }}>
                    <Text style={{
                        fontSize: 15,
                        color:COLORS.gray
                    }}>SubTotal</Text>
                    <Text style={{
                        fontSize: 25,
                        fontFamily: FONTS.bold,
                        color: COLORS.black
                    }}>PHP {total}
                    </Text>
                    <Text style={{
                        fontFamily: FONTS.regular,
                        fontSize: 12,
                        position: 'absolute',
                        right: 0,
                        top: 30
                }}>(Total does not include shipping)</Text>
                </View>
                <View style={{
                    marginTop: 20,
                    alignItems:'center',
                }}>
                    <TouchableOpacity 
                    onPress={() => {
                        if(cartItems.length === 0){
                            console.log("CArt is empty")
                        }else{
                            console.log("checkouted")
                        }
                    }}
                    activeOpacity={0.7}
                    style={{
                        backgroundColor: COLORS.transparentBlack7,
                        paddingHorizontal: SIZES.width / 3,
                        borderRadius: 20,
                        paddingVertical: 15
                    }}>
                        <Text style={{
                            color: COLORS.white
                        }}>Check Out</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    onPress={() => Navigation.navigate("Products", {})}
                    activeOpacity={0.7}
                    style={{
                        marginTop: 15
                    }}>
                        <Text>Continue Shopping</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
    },
    cartHeader:{
        fontFamily: FONTS.bold,
        fontSize: 22,
        color: COLORS.black
    },
    horizontalLine:{
        marginVertical: 10,
        width: SIZES.width * 0.9,
        height: 1,
        backgroundColor: COLORS.black,
        alignSelf: 'center'
    },
    negative:{
        marginRight: 10,
    },
    positive:{
        marginLeft: 10
    }
})

const mapStateToProps = (state: any) => ({
    cartItems: state.productReducer.cartItems,
    total: state.productReducer.total,
  });

  
export default connect(mapStateToProps)(Cart);
