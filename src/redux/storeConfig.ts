import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "@redux-saga/core";
import RootSaga from "./rootSaga";
import rootReducer from "./rootReducer";

const sagaMiddleWare = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleWare))
sagaMiddleWare.run(RootSaga)

export default store;