import { types } from "./types";

interface AuthTypes {
    isLoading: boolean,
    products: any,
    success: {},
    error: {},
    cartItems: [],
    total: number,
}

const initialState:AuthTypes = {
    isLoading: false,
    products: [
        {
            id: 1,
            productName: "Nick Dirt Bag",
            description: "A black creneck Black tee that shows your love for dirtbikes",
            sizeVarient: [
                "small",
                "medium",
                "large"
            ],
            defaultPrice: 750,
            prices:[{
                "small": 750,
                "medium": 780,
                "large": 800
            }],
            image: "https://cf.shopee.ph/file/d85b760130144e8266c46d999c65fcf7",
            modifier: "Featured",
            category: "T-shirt",
            rating: 3,
            stock: 0
        },
        {
            id: 2,
            productName: "Nick Automatic Flag",
            description: "Show your american side with this awesome nick automatic american inspired Tee",
            defaultPrice: 450,
            sizeVarient: [
                "small",
                "medium",
                "large"
            ],
            prices:[{
                "small": 450,
                "medium": 580,
                "large": 600
            }],
            image: "https://cf.shopee.ph/file/5ff9f927211fcd3d6b585e2e34f963cb",
            modifier: "Featured",
            category: "T-shirt",
            rating: 4,
            stock: 9
        },
        {
            id: 3,
            productName: "Nick Automatic The Thing",
            description: "A cool looking blue tee based on the monster from one of the most beloved movies in the 80s",
            defaultPrice: 450,
            sizeVarient: [
                "small",
                "medium",
                "large"
            ],
            prices:[{
                "small": 450,
                "medium": 580,
                "large": 600
            }],
            image: "https://cf.shopee.ph/file/47bab108e29e42ec2c142f3d1643b6a6",
            modifier: "Featured",
            category: "T-shirt",
            rating: 5,
            stock: 5
        },
        {
            id: 4,
            productName: "Nick Automatic Cosmos",
            description: "Represent a legacy of extraordinary hops. The Cosmos Jumpman Box T-Shirt features fresh brand graphics and the soft, comfortable feel of cotton.",
            defaultPrice: 450,
            sizeVarient: [
                "small",
                "medium",
                "large"
            ],
            prices:[{
                "small": 450,
                "medium": 580,
                "large": 600
            }],
            image: "https://cf.shopee.ph/file/88a1fede4639d2ab24dec895a6dcf38d",
            modifier: "Featured",
            category: "T-shirt",
            rating: 5,
            stock: 3
        },
        {
            id: 5,
            productName: "Umeboshi Ramen",
            description: "Show your love for japanese culture with this Gray and comfortable Jacket inspired by Japanese Design. in collaboration with umeboshi",
            defaultPrice: 1900,
            sizeVarient: [
                "small",
                "medium",
                "large"
            ],
            prices:[{
                "small": 1990,
                "medium": 2150,
                "large": 2160
            }],
            image: "https://cf.shopee.ph/file/da3c72648658ea8fa09031710a40db52",
            modifier: "Featured",
            category: "Jacket",
            rating: 4,
            stock: 5
        }
    ],
    success: {},
    error: {},
    cartItems: [],
    total: 0
}

const productReducer = (state = initialState, action:any) => {
    switch (action.type) {
        case types.REQUEST_PRODUCTS:
            return{
                ...state,
                isLoading: true,
            }
        case types.REQUEST_PRODUCTS_SUCCESS:
        return{
            ...state,
            isLoading: false,
            products: action.payload,
            error: {}
        }
        case types.REQUEST_PRODUCTS_FAILED:
            return{
                ...state,
                isLoading: false,
                products: {},
                error: action.payload
            }
        case types.ADD_TO_CART:
            const { id, image, price, quantity, varient, productName, stock} = action.payload;
            let selectedItem = { id, image, price, quantity, varient, productName, stock }
             //check if the action id and varient exists in the addedItems
            let itemInCart:any = state.cartItems.filter((item:any) => item.id === selectedItem.id && item.varient === selectedItem.varient)
            if(itemInCart.length)
                {
                    let existingQuantity = itemInCart[0].quantity += 1 
                     // look for existing Item element in addedItems (Cart/Item Holder)
                    const elementIndex = state.cartItems.findIndex((e:any) => e.id === itemInCart[0].id)
                    let existingItem = [itemInCart]; // Duplicate item
                     // update the item quantity
                     existingItem[elementIndex] = { ...existingItem[elementIndex], quantity: existingQuantity}
                    return{
                        ...state,
                        total: state.total + Number(itemInCart[0].price)
                          }
                }
            else{
                 //calculating the total
                let newTotal = state.total + (Number(selectedItem.price) * selectedItem.quantity)
                return{
                    ...state,
                    cartItems: [...state.cartItems, selectedItem],
                    total : newTotal
                } 
          }
          case types.ADD_QUANTITY: 
            let itemToIncrease:any = state.cartItems.filter((item:any) => item.id === action.payload.id && item.varient === action.payload.varient)
            itemToIncrease[0].quantity += 1 
            let newTotal = state.total + Number(itemToIncrease[0].price)
            return{
                ...state,
                total: newTotal
            }
         case types.DECREASE_QUANTITY:
            // I used action.payload since it contains the same object as items in cartItems
            const itemToDecrease:any = state.cartItems.filter((item:any) => item === action.payload);
            //if the qt == 0 then it should be removed

            if(itemToDecrease[0].quantity === 1){
                let newItems = state.cartItems.filter((x:any) => x !== itemToDecrease[0])
                let newTotal = state.total - Number(itemToDecrease[0].price)
                return{
                    ...state,
                    cartItems: newItems,
                    total: newTotal
                }
            }
            else {
                itemToDecrease[0].quantity -= 1
                let newTotals = state.total - Number(itemToDecrease[0].price)
                return{
                    ...state,
                    total: newTotals
                }
            }
            case types.REMOVE_ITEM: 
                // I used action.payload since it contains the same object as items in addeditems
                let itemToRemove:any = state.cartItems.filter((item:any) => item === action.payload);
                let new_items = state.cartItems.filter((item:any) => item !== itemToRemove[0])
                
                //calculating the total
                let updatedTotal = state.total - (Number(itemToRemove[0].price) * itemToRemove[0].quantity)
                return{
                    ...state,
                    cartItems: new_items,
                    total: updatedTotal
                }
        default:
            return state;
    }
}


export default productReducer;