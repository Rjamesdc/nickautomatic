import { takeLatest, put, call, delay } from "@redux-saga/core/effects";
import { types } from "./types";
import { asyncRequestProducts } from './action'
// import * as Navigation from '../../navigation/RootNavigation';

export interface RequestResponse {
    data: any,
    status: number,
    page?: number,
    perPage?: number, 
    length?: number | undefined,
    message?: any
  }
  
  function* getProducts(action:any) {
    const response:RequestResponse = yield call(asyncRequestProducts, action.payload);
    if(auth.status === 200){
      yield put({
        type: types.REQUEST_PRODUCTS_SUCCESS,
        payload: response
      })
    }else{
      yield put({
        type: types.REQUEST_PRODUCTS_FAILED,
        payload: response
      })
    }
  }  

  const productsSaga = [
    takeLatest(types.REQUEST_PRODUCTS, getProducts),
    // ...takeLatest 
  ];
  
  export default productsSaga;
  