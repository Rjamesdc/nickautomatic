import { types } from "./types";
import axios from "axios";

export interface RequestResponse {
    data: any,
    status: number,
    page?: number,
    perPage?: number,
    length?: number | undefined,
  }

export const asyncRequestProducts = async (params:any) => {
     try {
        const response: RequestResponse = await axios.post(`http://10.0.2.2:3333/api/v1/products`, params);
        return response
     } catch (error:any) {
         return error.response
     }
}