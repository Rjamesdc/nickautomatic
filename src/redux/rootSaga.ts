import {take, call, put, all} from "@redux-saga/core/effects";
// sagas
import productsSaga from "./Products/saga";

export default function* RootSaga(){
    yield all([
        ...productsSaga
    ])
}