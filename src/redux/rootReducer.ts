import { combineReducers } from "redux";
import productReducer from "./Products/reducer";

const rootReducer = combineReducers({
    // reducers
    productReducer,
})


export default rootReducer;